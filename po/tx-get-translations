#!/bin/bash

PODIR=po
LINGUAS=$PODIR/LINGUAS
[ ! -f $LINGUAS ] && echo "Warning: No LINGUAS file found in $PODIR : exit" && exit 1
LANGS=($( grep -v '^\s*#' $PODIR/LINGUAS 2>/dev/null))
((${#LANGS[@]}==0)) && echo "Warning: No languages found in $LINGUAS; exit" && exit 1

# need 'latest' transifex client
: ${TXBIN:=$(which tx)}
[ ! -x "$TXBIN" ] && echo "Error: transifex client not found!" && exit 1

MINIMUM_PERC=${2:-${MINIMUM_PERC:=0}}

# set minium translations completion in percent to pull translation
: ${MINIMUM_PERC:=0}
export MINIMUM_PERC

# set transifex organization and project name - if not set in environment already
: ${ORGANIZATION:=anticapitalista}
: ${PROJECT:=antix-development}
RESOURCE=live-bootloader
RESOUCE_ID="${PROJECT}.${RESOURCE}"
POTFILE=bootloader.pot


# prepare transifex 
[ -d .tx         ] || mkdir -p .tx
[ -f  .tx/config ] && rm  .tx/config

cat <<EOF | tee .tx/config
[main]
host = https://app.transifex.com

[o:${ORGANIZATION}:p:${PROJECT}:r:${RESOURCE}]

file_filter = ${PODIR}/<lang>.po
minimum_perc = ${MINIMUM_PERC:=0}
resource_name = ${RESOURCE}
source_file = ${POTFILE}
source_lang = en
type = PO
EOF

echodo() { echo "${@}";  ${@}; }

# backup existing
[ -d ${PODIR} ] && echodo cp -a  ${PODIR} ${PODIR}-$(date '+%y%m.%d-%H%M%S').BAK~

unset LINGUAS_MOVE
declare -A LINGUAS_MOVE
eval LINGUAS_MOVE=( $(grep -v '^\s*#' $PODIR/LINGUAS.move 2>/dev/null) )
declare -p LINGUAS_MOVE

declare -A LINGUAS_COPY
eval LINGUAS_COPY=( $(grep -v '^\s*#' $PODIR/LINGUAS.copy 2>/dev/null) )
declare -p LINGUAS_COPY

LANGUAGES="--all"
LANGUAGES="${LANGS[*]}"
LANGUAGES="${LANGUAGES// /,}"
LANGUAGES="--languages $LANGUAGES"

echodo ${TXBIN} pull --force  --translations $LANGUAGES "$RESOUCE_ID"

for lang in ${!LINGUAS_MOVE[@]}; do
	map=${LINGUAS_MOVE[$lang]}
	po=$PODIR/$lang.po
	np=$PODIR/$map.po
	if [ -f $po ]; then
		echodo mv $po $np
	fi
done

for lang in ${!LINGUAS_COPY[@]}; do
	map=${LINGUAS_COPY[$lang]}
	po=$PODIR/$lang.po
	np=$PODIR/$map.po
	if [ -f $po ]; then
		echodo cp $po $np
	fi
done

[ -x ./tx-translation-completness-log ] &&  ./tx-translation-completness-log ${PODIR}
echo
echo Done: ${TXBIN} pull --force --translations $LANGUAGES "$RESOUCE_ID"
