# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Ivan <personal@live.hk>, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-04 21:24+0200\n"
"PO-Revision-Date: 2021-07-09 16:40+0000\n"
"Last-Translator: Ivan <personal@live.hk>, 2022\n"
"Language-Team: Chinese (Hong Kong) (https://app.transifex.com/anticapitalista/teams/10162/zh_HK/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: zh_HK\n"
"Plural-Forms: nplurals=1; plural=0;\n"

msgid "Advanced Options"
msgstr "顯示進階選項"

msgid "Back to main menu"
msgstr "返回主頁"

msgid "Back to main menu (or press »ESC«)"
msgstr "返回主頁 (或按 »ESC«)"

msgid "Boot Process Performance Visualization"
msgstr "啟動過程性能可視化"

msgid "Boot Rescue Menus"
msgstr "啟動修復選項"

msgid "Boot options"
msgstr "開機選項"

msgid "Change passwords before booting"
msgstr "在開機前更改密碼"

msgid "Check LiveUSB and persistence ext2/3/4 file systems"
msgstr "檢查 LiveUSB 和持久性 ext2/3/4 文件系統"

msgid "Check integrity of the live media"
msgstr "檢查實時媒體的完整性"

msgid "Console options"
msgstr "Console options"

msgid "Copy the compressed file system to RAM"
msgstr "Copy the compressed file system to RAM"

msgid "Custom"
msgstr "Custom"

msgid "Customize Boot (text menus)"
msgstr "Customize Boot (text menus)"

msgid "Desktop options"
msgstr "Desktop options"

msgid "Disable ACPI"
msgstr "Disable ACPI"

msgid "Disable GRUB theme"
msgstr "Disable GRUB theme"

msgid "Disable Intel graphics invert"
msgstr "Disable Intel graphics invert"

msgid "Disable LiveUSB-Storage feature"
msgstr "Disable LiveUSB-Storage feature"

msgid "Disable automount via fstab"
msgstr "Disable automount via fstab"

msgid "Disable console width"
msgstr "Disable console width"

msgid "Disable dual video card detection"
msgstr "Disable dual video card detection"

msgid "Disable swap"
msgstr "Disable swap"

msgid "Disable text-splash screen"
msgstr "Disable text-splash screen"

msgid "Don't look for USB-2 devices"
msgstr "Don't look for USB-2 devices"

msgid "Don't save files across reboots"
msgstr "Don't save files across reboots"

msgid "Don't set repositories based on timezone."
msgstr "Don't set repositories based on timezone."

msgid "EFI Bootloader"
msgstr "EFI Bootloader"

msgid "Enable EFI subsystem for RT-PREEMPT kernels"
msgstr "Enable EFI subsystem for RT-PREEMPT kernels"

msgid "Enable GRUB theme"
msgstr "Enable GRUB theme"

msgid "Enable LiveUSB-Storage feature"
msgstr "Enable LiveUSB-Storage feature"

msgid "Enable automount via fstab"
msgstr "Enable automount via fstab"

msgid "Enable dual video card detection"
msgstr "Enable dual video card detection"

msgid "Enable text-splash screen"
msgstr "Enable text-splash screen"

msgid "Failsafe options"
msgstr "Failsafe options"

msgid "Finish booting from a LiveUSB"
msgstr "Finish booting from a LiveUSB"

msgid "Finish booting from a LiveUSB or hard drive"
msgstr "Finish booting from a LiveUSB or hard drive"

msgid "Finish booting from a hard drive"
msgstr "Finish booting from a hard drive"

msgid "Frugal Menus"
msgstr "Frugal Menus"

msgid "Frugal like p_static_root"
msgstr "Frugal like p_static_root"

msgid "Frugal like persist_all"
msgstr "Frugal like persist_all"

msgid "Frugal like persist_home"
msgstr "Frugal like persist_home"

msgid "Frugal like persist_root"
msgstr "Frugal like persist_root"

msgid "Frugal like persist_static"
msgstr "Frugal like persist_static"

msgid "Frugal menu"
msgstr "Frugal menu"

msgid "Frugal menus"
msgstr "Frugal menus"

msgid "GFX menu and GRUB menu"
msgstr "GFX menu and GRUB menu"

msgid "GRUB Bootloader"
msgstr "GRUB Bootloader"

msgid "GRUB Menus"
msgstr "GRUB Menus"

msgid "GRUB bootloader"
msgstr "GRUB bootloader"

msgid "GRUB loader"
msgstr "GRUB loader"

msgid "GRUB menu"
msgstr "GRUB menu"

msgid "GRUB menus"
msgstr "GRUB menus"

msgid "GRUB theme"
msgstr "GRUB theme"

msgid "GRUB-EFI bootloader"
msgstr "GRUB-EFI bootloader"

msgid "Hardware clock uses UTC (Linux)"
msgstr "Hardware clock uses UTC (Linux)"

msgid "Hardware clock uses local time (Windows)"
msgstr "Hardware clock uses local time (Windows)"

msgid "Help"
msgstr "Help"

msgid "Invert video on some Intel graphics systems"
msgstr "Invert video on some Intel graphics systems"

msgid "Kernel options"
msgstr "Kernel options"

msgid "Keyboard"
msgstr "Keyboard"

msgid "Language"
msgstr "Language"

msgid "Memory Test"
msgstr "Memory Test"

msgid "No EFI bootloader found."
msgstr "No EFI bootloader found."

msgid "No Frugal menu found."
msgstr "No Frugal menu found."

msgid "No GRUB CFG-Menu found."
msgstr "No GRUB CFG-Menu found."

msgid "No GRUB bootloader found."
msgstr "No GRUB bootloader found."

msgid "No Shim-EFI bootloader found."
msgstr "No Shim-EFI bootloader found."

msgid "No Windows bootloader found."
msgstr "No Windows bootloader found."

msgid "Only Frugal, no persistence"
msgstr "Only Frugal, no persistence"

msgid "Only »/home« on persistence device"
msgstr "Only »/home« on persistence device"

msgid "Persistence option"
msgstr "Persistence option"

msgid "Power Off"
msgstr "Power Off"

msgid "Press <Enter> to continue"
msgstr "Press <Enter> to continue"

msgid "Press »Enter« to continue"
msgstr "Press »Enter« to continue"

msgid "Press »e« to edit, »ESC« to go back."
msgstr "Press »e« to edit, »ESC« to go back."

msgid "Reboot"
msgstr "Reboot"

msgid "Reboot into BIOS/UEFI Setup"
msgstr "Reboot into BIOS/UEFI Setup"

msgid "Reset"
msgstr "重置"

msgid "Save options"
msgstr "儲存選項"

msgid "Save options (LiveUSB only)"
msgstr "儲存選項 (只限 LiveUSB )"

msgid "Save some files across reboots"
msgstr "Save some files across reboots"

msgid "Secure Boot Restrictions"
msgstr "Secure Boot Restrictions"

msgid "Shim-EFI bootloader"
msgstr "Shim-EFI bootloader"

msgid "Show text menus"
msgstr "Show text menus"

msgid "Show video card detection menu"
msgstr "Show video card detection menu"

msgid "Switch to Syslinux"
msgstr "Switch to Syslinux"

msgid "The highlighted entry will start in %d seconds."
msgstr "The highlighted entry will start in %d seconds."

msgid "Timezone"
msgstr "時區"

msgid "UTC or local time is asked"
msgstr "已詢問 UTC 或當地時間"

msgid "Use ↑ and ↓. Hit »ENTER« to select/deselect."
msgstr "Use ↑ and ↓. Hit »ENTER« to select/deselect."

msgid "Windows Bootloader"
msgstr "Windows Bootloader"

msgid "Windows bootloader"
msgstr "Windows bootloader"

msgid "Windows bootloader on drive"
msgstr "Windows bootloader on drive"

msgid "disabled"
msgstr "disabled"

msgid "enabled"
msgstr "enabled"

msgid "finished"
msgstr "finished"

msgid "found EFI bootloader at"
msgstr "found EFI bootloader at"

msgid "found Frugal menu at"
msgstr "found Frugal menu at"

msgid "found GRUB bootloader at"
msgstr "found GRUB bootloader at"

msgid "found GRUB menu on"
msgstr "found GRUB menu on"

msgid "found Windows bootloader on drive"
msgstr "found Windows bootloader on drive"

msgid "root »/« and »/home« in RAM"
msgstr "root »/« and »/home« in RAM"

msgid "root »/« and »/home« separate on persistence device"
msgstr "root »/« and »/home« separate on persistence device"

msgid "root »/« and »/home« together on persistence device"
msgstr "root »/« and »/home« together on persistence device"

msgid "root »/« in RAM, »/home« on persistence device"
msgstr "root »/« in RAM, »/home« on persistence device"

msgid "searching for Frugal grub.entry menus"
msgstr "searching for Frugal grub.entry menus"

msgid "searching for GRUB bootloader"
msgstr "searching for GRUB bootloader"

msgid "searching for GRUB menus"
msgstr "searching for GRUB menus"

msgid "searching for GRUB-EFI bootloader"
msgstr "searching for GRUB-EFI bootloader"

msgid "searching for Shim-EFI bootloader"
msgstr "searching for Shim-EFI bootloader"

msgid "searching for Windows bootloader"
msgstr "searching for Windows bootloader"

msgid "version"
msgstr "version"

msgid "Disable remastering even if linuxfs.new is found."
msgstr "Disable remastering even if linuxfs.new is found."

msgid "Roll back to the previous remastered version."
msgstr "Roll back to the previous remastered version."

msgid "Show frugal device selection - can be saved"
msgstr "Show frugal device selection - can be saved"

msgid "Show frugal device selection - one time, not saved"
msgstr "Show frugal device selection - one time, not saved"

msgid "found Windows bootloader on"
msgstr "found Windows bootloader on"

msgid ""
"Minimal BASH-like line editing is supported. For the first word, TAB lists "
"possible command completions. Anywhere else TAB lists possible device or "
"file completions. %s"
msgstr ""
"Minimal BASH-like line editing is supported. For the first word, TAB lists "
"possible command completions. Anywhere else TAB lists possible device or "
"file completions. %s"

msgid ""
"Minimum Emacs-like screen editing is supported. TAB lists completions. Press"
" Ctrl-x or F10 to boot, Ctrl-c or F2 for a command-line or ESC to discard "
"edits and return to the GRUB menu."
msgstr ""
"Minimum Emacs-like screen editing is supported. TAB lists completions. Press"
" Ctrl-x or F10 to boot, Ctrl-c or F2 for a command-line or ESC to discard "
"edits and return to the GRUB menu."

msgid "ESC at any time exits."
msgstr "以 ESC 隨時退出。"
